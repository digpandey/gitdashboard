 import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NabbarComponent } from './nabbar/nabbar.component';
import { MainComponent } from './main/main.component';
 import { Ng2SearchPipeModule } from 'ng2-search-filter';
//  import { Ng2OrderModule } from 'ng2-order-pipe';
 import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    NabbarComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
     Ng2SearchPipeModule,
    // Ng2OrderModule, 
     NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
