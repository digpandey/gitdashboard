import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import * as XLSX from 'xlsx';
declare var $;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
    
    key:string[];
    dataSheet:any[]
    @ViewChild('dataTable') table;
    isSaved:boolean=false;
  dataTable: any
  constructor() { }

  ngOnInit(): void {
   
  }
  onClick(){
    this.isSaved=true;
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable();
    
    
  }
  onFileChange(evt)
  {
    let data;
  const target:DataTransfer=  <DataTransfer>(evt.target);
  
  const reader:FileReader=new FileReader();
  reader.onload=(e:any)=>{
    debugger;
    const bstr:string=e.target.result;
    const wb:XLSX.WorkBook=XLSX.read(bstr,{type:'binary'});
    const wsname:string=wb.SheetNames[0];
    const ws:XLSX.WorkSheet=wb.Sheets[wsname];
   data=XLSX.utils.sheet_to_json(ws);
  
  };
  reader.readAsBinaryString(target.files[0]);
  reader.onloadend=(e)=>{
    
    this.key=Object.keys(data[0]);
    this.dataSheet=Object.values(data);
    
    
  };
  
  
  

  
  }
  
  applyCss()
  {
    
    
  }

}
